<?php

namespace App\Controller;

use App\Entity\Ciudades;
use App\Entity\Usuarios;
use App\Entity\Demos;
use App\Entity\Opiniones;
use App\Entity\Pasos;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class MaleteoController extends AbstractController
{
    /**
     * @Route("/login", name="loginMaleteo")
     */
    public function login(Request $request, EntityManagerInterface $em)
    {
        
        $usuario = $request->get('usuario');
        $contrasenna = $request->get('contrasenna');

        $repo = $em->getRepository(Usuarios::class);

        $datosUsuarios = $repo->findOneBy(["nombreUsuario" => $usuario, "contrasenna" => $contrasenna]);

        if($datosUsuarios) { 

            $session = new Session();
            $session->start();
            $session->set("id", $datosUsuarios->getId());
            $session->set("usuario", $datosUsuarios->getNombreUsuario());
            $session->set("lugar", $datosUsuarios->getCiudad());

            return $this->redirectToRoute("indexMaleteo");
        }

        return $this->render("login.html.twig");
 
    }

    /**
     * @Route("/maleteo", name="indexMaleteo")
     */
    public function homePage(Request $request, EntityManagerInterface $em)
    {
        $nombre = $request->get("nombre");
        $email = $request->get("email");
        $ciudades = $request->get("ciudad");
        $checkbox = $request->get("privacidad");

        $session = new Session();
        $user = $session->get("usuario");
        $pass = $session->get("contrassena");
        
        if ($checkbox == 1) {
            $demo = new Demos();
            $demo->setNombre($nombre);
            $demo->setEmail($email);
            $demo->setCiudad($ciudades);
            $demo->setPolitica($checkbox);
            $em->persist($demo);
            $em->flush();
        }

        $repo = $em->getRepository(Opiniones::class);
        $opiniones = $repo->findAll();

        $repo2 = $em->getRepository(Ciudades::class);
        $ciudades = $repo2->findAll();

        $repo3 = $em->getRepository(Pasos::class);
        $pasos = $repo3->findAll();

        return $this->render('maleteo.html.twig', ["opiniones" => $opiniones, "ciudades" => $ciudades, "pasos" => $pasos, "user" => $user, "pass" => $pass]);
    }

    /**
     * @Route("/opiniones", name="pag-opiniones")
     */

    public function setOpinion(Request $request, EntityManagerInterface $em)
    {
        $session = new Session();
        $id = $session->get("id");
        $usuario = $session->get("usuario");
        $lugar = $session->get("lugar");
        $opinion = $request->get("opinion");

        $repo = $em->getRepository(Opiniones::class);
        
        $opinionUsuario = $repo->findOneBy(["idUsuario" => $id]);

        if ($opinion) {
            $objOpinion = new Opiniones();
            $objOpinion->setOpinion($opinion);
            $objOpinion->setPersona($usuario);
            $objOpinion->setLugar($lugar);
            $objOpinion->setIdUsuario($id);
            $em->persist($objOpinion);
            $em->flush();
        }
        return $this->render('opinion.html.twig', ["opinionUsuario" => $opinionUsuario]);
    }

    /**
     * @Route("registro")
     */
    public function registro(Request $request, EntityManagerInterface $em) {
        $nombre = $request->get("nombre");
        $email = $request->get("email");
        $contrasenna = $request->get("contrasenna");
        $cp = $request->get("cp");
        $fechaNacimiento = $request->get("fechaNac");
        $date = DateTime::createFromFormat('Y-m-d', $fechaNacimiento);
        $telefono = $request->get("tlfn");
        $ciudad = $request->get("ciudad");
        $checkbox = $request->get("privacidad");

        if($checkbox == 1){
            $usuario = new Usuarios();
            $usuario->setNombreUsuario($nombre);
            $usuario->setEmail($email);
            $usuario->setContrasenna($contrasenna);
            $usuario->setCodigoPostal($cp);
            $usuario->setFechaNacimiento($date);
            $usuario->setTelefono($telefono);
            $usuario->setCiudad($ciudad);
            $em->persist($usuario);
            $em->flush();
        }

        return $this->render('registro.html.twig');
    }

    /**
     * @Route("/logout")
     */
    public function logOut(){
        $session = new Session();
        $session->clear();

        return $this->redirectToRoute("indexMaleteo");
    }
}
