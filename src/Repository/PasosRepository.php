<?php

namespace App\Repository;

use App\Entity\Pasos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Pasos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pasos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pasos[]    findAll()
 * @method Pasos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PasosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pasos::class);
    }

    // /**
    //  * @return Pasos[] Returns an array of Pasos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pasos
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
