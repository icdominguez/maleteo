<?php

namespace App\Repository;

use App\Entity\Demos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Demos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Demos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Demos[]    findAll()
 * @method Demos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Demos::class);
    }

    // /**
    //  * @return Demos[] Returns an array of Demos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Demos
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
