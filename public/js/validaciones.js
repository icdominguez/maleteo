function validarAlEnviar(){
    try {
        let resultado = validarNombre() && validarEmail();
        return resultado;
    } catch (ex) {
        return false;
    }
}

function validarNombre(){
    inputNombre = document.getElementById("nombre");
    if (inputNombre.value.length < 3){
        inputNombre.setAttribute("class", "form-input-invalid");
        return false;
    }
    else{
        inputNombre.setAttribute("class", "form-input-valid");
        return true;
    }
}

function validarEmail(){
    
    let inputEmail = document.getElementById("email");

    let posicionDireccion = inputEmail.value.lastIndexOf("@");
    let email = inputEmail.value.substr(posicionDireccion+1);
    let posicionDominio = email.lastIndexOf(".");
    let dominio = email.substr(posicionDominio+1);

    if(dominio.length < 2 || dominio.length > 4){
        inputEmail.setAttribute("class", "form-input-invalid");
        return false;
        
    }else{
        inputEmail.setAttribute("class", "form-input-valid");
        return true;
    }
}

function validarTelefono(){
    let inputTlf = document.getElementById("tlfn");
    let objExpReg = new RegExp("^[0-9]{6,9}$");

    if(!objExpReg.test(inputTlf.value)){
        inputTlf.setAttribute("class", "form-input-invalid");
        inputTlf.style.display = "block";
        return false;
    }else{
        inputTlf.setAttribute("class", "form-input-valid");
        return true;
    }
}

function validarCP(){
    let inputCP = document.getElementById("cp");
    if(inputCP.value.length == 5 && inputCP.value >= 1000 && inputCP.value <= 52999){
        inputCP.setAttribute("class","form-input-valid");
        return true;
    }else{
        inputCP.setAttribute("class","form-input-invalid");
        return false;
    }
}

function validarEdad(){
    let edad = document.getElementById("fechaNac");
    if(edad.value > "2002/01/01" || edad.value == ""){
        edad.setAttribute("class","form-input-invalid");
        return false;
    }else{
        edad.setAttribute("class","form-input-valid");
        return true;
    }
}

window.onload = function(){
    document.getElementById("nombre").onkeyup = validarNombre;
    document.getElementById("nombre").onblur = validarNombre;
    document.getElementById("email").onblur = validarEmail;
    document.getElementById("tlfn").onblur = validarTelefono;  
    document.getElementById("cp").onblur = validarCP;
    document.getElementById("fechaNac").onblur = validarEdad;
}